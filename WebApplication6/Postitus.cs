//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WebApplication6
{
    using System;
    using System.Collections.Generic;
    
    public partial class Postitus
    {
        public int Id { get; set; }
        public int KasutajaId { get; set; }
        public System.DateTime Kuupäev { get; set; }
        public string Sisu { get; set; }
    
        public virtual Kasutaja Kasutaja { get; set; }
    }
}
