﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication6;

namespace WebApplication6.Controllers
{
    public class PostitusController : Controller
    {

        private HennuOmaEntities db = new HennuOmaEntities();
        

        // GET: Postitus
        public ActionResult Index()
        {
            var postitus = db.Postitus.Include(p => p.Kasutaja);
            return View(postitus.ToList());
        }

        // GET: Postitus/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Postitus postitus = db.Postitus.Find(id);
            if (postitus == null)
            {
                return HttpNotFound();
            }
            return View(postitus);
        }

        // GET: Postitus/Create
        public ActionResult Create()
        {
            //ViewBag.KasutajaNimi = db.Kasutaja.Where(x => x.Email == User.Identity.Name).SingleOrDefault()?.Nimi;
                        ViewBag.KasutajaId = new SelectList(db.Kasutaja, "Id", "Nimi");
            Postitus p = new Postitus { KasutajaId = db.Kasutaja.Where(x => x.Email == User.Identity.Name).SingleOrDefault()?.Id ?? 0, Kuupäev = DateTime.Now.Date };

            return View(p);
        }

        // POST: Postitus/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,KasutajaId,Kuupäev,Sisu")] Postitus postitus)
        {
            if (ModelState.IsValid)
            {
//                postitus.KasutajaId = db.Kasutaja.Where(x => x.Email == User.Identity.Name).SingleOrDefault()?.Id ?? 0;
                db.Postitus.Add(postitus);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.KasutajaId = new SelectList(db.Kasutaja, "Id", "Nimi", postitus.KasutajaId);
            return View(postitus);
        }

        // GET: Postitus/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Postitus postitus = db.Postitus.Find(id);
            if (postitus == null)
            {
                return HttpNotFound();
            }
            ViewBag.KasutajaId = new SelectList(db.Kasutaja, "Id", "Nimi", postitus.KasutajaId);
            return View(postitus);
        }

        // POST: Postitus/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,KasutajaId,Kuupäev,Sisu")] Postitus postitus)
        {
            if (ModelState.IsValid)
            {
                db.Entry(postitus).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.KasutajaId = new SelectList(db.Kasutaja, "Id", "Nimi", postitus.KasutajaId);
            return View(postitus);
        }

        // GET: Postitus/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Postitus postitus = db.Postitus.Find(id);
            if (postitus == null)
            {
                return HttpNotFound();
            }
            return View(postitus);
        }

        // POST: Postitus/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Postitus postitus = db.Postitus.Find(id);
            db.Postitus.Remove(postitus);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
