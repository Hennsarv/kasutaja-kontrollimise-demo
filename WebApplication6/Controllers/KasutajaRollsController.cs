﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication6;

namespace WebApplication6.Controllers
{
    public class KasutajaRollsController : Controller
    {
        private HennuOmaEntities db = new HennuOmaEntities();

        // GET: KasutajaRolls
        public ActionResult Index()
        {
            var kasutajaRoll = db.KasutajaRoll.Include(k => k.Kasutaja).Include(k => k.Roll1);
            return View(kasutajaRoll.ToList());
        }

        // GET: KasutajaRolls/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            KasutajaRoll kasutajaRoll = db.KasutajaRoll.Find(id);
            if (kasutajaRoll == null)
            {
                return HttpNotFound();
            }
            return View(kasutajaRoll);
        }

        // GET: KasutajaRolls/Create
        public ActionResult Create()
        {
            ViewBag.KasutajaId = new SelectList(db.Kasutaja, "Id", "Nimi");
            ViewBag.Roll = new SelectList(db.Roll, "Id", "Nimetus");
            return View();
        }

        // POST: KasutajaRolls/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,KasutajaId,Roll")] KasutajaRoll kasutajaRoll)
        {
            if (ModelState.IsValid)
            {
                db.KasutajaRoll.Add(kasutajaRoll);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.KasutajaId = new SelectList(db.Kasutaja, "Id", "Nimi", kasutajaRoll.KasutajaId);
            ViewBag.Roll = new SelectList(db.Roll, "Id", "Nimetus", kasutajaRoll.Roll);
            return View(kasutajaRoll);
        }

        // GET: KasutajaRolls/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            KasutajaRoll kasutajaRoll = db.KasutajaRoll.Find(id);
            if (kasutajaRoll == null)
            {
                return HttpNotFound();
            }
            ViewBag.KasutajaId = new SelectList(db.Kasutaja, "Id", "Nimi", kasutajaRoll.KasutajaId);
            ViewBag.Roll = new SelectList(db.Roll, "Id", "Nimetus", kasutajaRoll.Roll);
            return View(kasutajaRoll);
        }

        // POST: KasutajaRolls/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,KasutajaId,Roll")] KasutajaRoll kasutajaRoll)
        {
            if (ModelState.IsValid)
            {
                db.Entry(kasutajaRoll).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.KasutajaId = new SelectList(db.Kasutaja, "Id", "Nimi", kasutajaRoll.KasutajaId);
            ViewBag.Roll = new SelectList(db.Roll, "Id", "Nimetus", kasutajaRoll.Roll);
            return View(kasutajaRoll);
        }

        // GET: KasutajaRolls/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            KasutajaRoll kasutajaRoll = db.KasutajaRoll.Find(id);
            if (kasutajaRoll == null)
            {
                return HttpNotFound();
            }
            return View(kasutajaRoll);
        }

        // POST: KasutajaRolls/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            KasutajaRoll kasutajaRoll = db.KasutajaRoll.Find(id);
            db.KasutajaRoll.Remove(kasutajaRoll);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
