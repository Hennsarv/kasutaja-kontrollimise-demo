﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication6.Controllers
{
    public class HomeController : Controller
    {
        private HennuOmaEntities db = new HennuOmaEntities();
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";
            if(Request.IsAuthenticated)
            {
                ViewBag.Kasutajanimi = db.Kasutaja.Where(x => x.Email == User.Identity.Name).SingleOrDefault()?.Nimi ?? "pole meie kasutaja";
            }
            else
            {
                ViewBag.Kasutajanimi = "";
            }

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}